(define-package "highlight-indent-guides" "20161230.1538" "Minor mode to highlight indentation" '((emacs "24.4")) :url "https://github.com/DarthFennec/highlight-indent-guides")
