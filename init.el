;; Package --- Emacs Config
(setq user-full-name "Jonathan Stefani")
(setq user-mail-address "jon.stefani@gmail.com")

(load-file "~/.emacs.d/elispConfigFiles/sensible-defaults.el")
(sensible-defaults/use-all-settings)
(sensible-defaults/use-all-keybindings)

;; Replace Splash Screen with org-mode scratch buffer ;;
(setq inhibit-splash-screen t
	  initial-scratch-message nil
	  initial-major-mode 'org-mode)

;; Package Managers
(require 'package)
(setq package-enable-at-startup nil)
(package-initialize)
(add-to-list 'package-archives
	     '("marmalade" . "http://marmalade-repo.com/packages/"))
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(setq package-archive-enable-alist '(("melpa" deft magit)))

;; Gui configurations
(tool-bar-mode -1)
(scroll-bar-mode -1)
(global-linum-mode t)
(blink-cursor-mode -1)
(fringe-mode 6)
(global-hl-line-mode t)
; show colum number and line number
(dolist (mode '(column-number-mode line-number-mode))
  (when (fboundp mode) (funcall mode t)))
(dolist (mode-hook '(text-mode-hook prog-mode-hook))
  (add-hook mode-hook
	    (lambda ()
	      (linum-mode 1))))
; turns off the damn annoying bells
(setq ring-bell-function 'ignore)
(setq w32-pipe-read-delay 0)

;; Font Settings
(set-frame-font "Inconsolata 14" nil t)

(set-language-environment "UTF-8")

(show-paren-mode 1)
(setq show-paren-delay 0)
(global-prettify-symbols-mode t)
(setq scroll-conservatively 100)
(global-subword-mode 1)
(setq-default tab-width 4)
(global-subword-mode 1)
(setq compilation-scroll-output t)

;; Theme ;;
(when window-system
  (setq solarized-use-variable-pitch nil)
  (setq solarized-height-plus-1 1.0)
  (setq solarized-height-plus-2 1.0)
  (setq solarized-height-plus-3 1.0)
  (setq solarized-height-plus-4 1.0)
  (setq solarized-high-contrast-mode-line t)
  (load-theme 'solarized-dark t))

;; Mode Icons config ;;
(mode-icons-mode 1)

;; Powerline Config ;;
(require 'powerline)
(powerline-center-theme)

;; Key Bindings ;;
(global-set-key (kbd "C-x p") 'eval-buffer)
(global-set-key (kbd "C-x n") 'beginning-of-line)
(global-set-key (kbd "C-x u") 'end-of-line)

;; Switch windows when you split the screen ;;
(defun split-window-below-and-switch ()
  "Split the window horizontally and then switch to the new pane"
  (interactive)
  (split-window-below)
  (other-window 1))

(defun split-window-right-and-switch ()
  "Split the window vertically and then switch to the new pane"
  (interactive)
  (split-window-right)
  (other-window 1))

(global-set-key (kbd "C-x 2") 'split-window-below-and-switch)
(global-set-key (kbd "C-x 3") 'split-window-right-and-switch)

;####################################;
; -----  Package Settings      ----- ;
;####################################;

;; Electric Pair, matchs parenthesis, etc. ;;
(electric-pair-mode 1)

;; Company Mode Config ;;
(require 'req-package)
(req-package company
	     :require yasnippet
	     :config (progn (global-company-mode 1)
			    (setq company-idle-delay 0)
			    (setq company-show-numbers t)
			    (setq company-minimum-prefix-length 2)
			    (setq company-dabbrev-downcase nil)
			    (setq company-dabbrev-other-buffers t)
			    (setq company-auto-complete nil)
			    (setq company-dabbrev-code-other-buffer 'all)
			    (setq company-code-everywhere t)
			    (setq company-code-ignore-case t)
			    (global-set-key (kbd "C-<tab>") 'company-dabbrev)
			    (global-set-key (kbd "M-<tab>") 'company-complete)
			    (global-set-key (kbd "C-c C-y") 'company-yasnippet)))

(req-package company-quickhelp
	     :require company
	     :config (company-quickhelp-mode 1))

(provide 'init-company)
(add-hook 'after-init-hook 'global-company-mode)

;; ido and smex config ;;
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)
(ido-ubiquitous)
(flx-ido-mode 1)
(setq ido-create-new-buffer 'always)
(ido-vertical-mode 1)
(setq ido-vertical-define-keys 'C-n-and-C-p-only)

; the smex part
(smex-initialize)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)

(require 'smooth-scrolling)
(smooth-scrolling-mode 1)

;; The Glorious and Wonderful Git Config ;;
(require 'git-gutter)
(global-git-gutter-mode 1)

;; Web Mode Config ;;
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

;; Tern Config ;;
(eval-after-load 'tern
  '(progn
     (require 'tern-auto-complete)
     (tern-ac-setup)))

;; Web Beautify ;;
(eval-after-load 'js2-mode
  '(add-hook 'js2-mode-hook
             (lambda ()
               (add-hook 'before-save-hook 'web-beautify-js-buffer t t))))

;; Or if you're using 'js-mode' (a.k.a 'javascript-mode')
(eval-after-load 'js
  '(add-hook 'js-mode-hook
             (lambda ()
               (add-hook 'before-save-hook 'web-beautify-js-buffer t t))))

(eval-after-load 'json-mode
  '(add-hook 'json-mode-hook
             (lambda ()
               (add-hook 'before-save-hook 'web-beautify-js-buffer t t))))

(eval-after-load 'sgml-mode
  '(add-hook 'html-mode-hook
             (lambda ()
               (add-hook 'before-save-hook 'web-beautify-html-buffer t t))))

(eval-after-load 'web-mode
  '(add-hook 'web-mode-hook
             (lambda ()
               (add-hook 'before-save-hook 'web-beautify-html-buffer t t))))

(eval-after-load 'css-mode
  '(add-hook 'css-mode-hook
             (lambda ()
               (add-hook 'before-save-hook 'web-beautify-css-buffer t t))))

;; Css/Scss config ;;
(add-hook 'css-mode-hook
	  (lambda ()
	    (rainbow-mode)
	    (setq css-indent-offset 2)))
(add-hook 'scss-mode-hook 'rainbow-mode)
(setq scss-compile-at-save nil)

;; Highlight Mode Config ;;
(auto-highlight-symbol-mode 1)

;; White Space Clean Up Mode Config ;;
(global-whitespace-cleanup-mode 1)

;; Discover Mode Config ;;
(require 'discover)
(global-discover-mode)

;; Oh Glorious and Wonderful Orgmode config ;;
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(require 'org-alert)
(setq alert-default-style 'libnotify)

;; Rust Config ;;
(setq racer-rust-src-path "/home/jstefani/Documents/rust-source/rustc/src")
(add-hook 'rust-mode-hook #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)
(add-hook 'racer-mode-hook #'company-mode)

(require 'rust-mode)
(define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
(setq company-tooltip-align-annotations t)

;; C/C++ Config ;;
(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)

(add-hook 'c++-mode-hook 'company-mode)
(add-hook 'c-mode-hook 'company-mode)

(defun my-irony-mode-hook ()
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))
(add-hook 'irony-mode-hook 'my-irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))
(add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)

;; eldoc irony ;;
(add-hook 'irony-mode-hook 'irony-eldoc)

;; binds tab to tab or complete
(defun irony--check-expansion ()
(save-excursion
  (if (looking-at "\\_>") t
    (backward-char 1)
    (if (looking-at "\\.") t
      (backward-char 1)
      (if (looking-at "->") t nil)))))
(defun irony--indent-or-complete ()
"Indent or Complete"
(interactive)
(cond ((and (not (use-region-p))
            (irony--check-expansion))
       (message "complete")
       (company-complete-common))
      (t
       (message "indent")
       (call-interactively 'c-indent-line-or-region))))
(defun irony-mode-keys ()
"Modify keymaps used by `irony-mode'."
(local-set-key (kbd "TAB") 'irony--indent-or-complete)
(local-set-key [tab] 'irony--indent-or-complete))
(add-hook 'c-mode-common-hook 'irony-mode-keys)

;; Indententation Guide Setup ;;
(require 'highlight-indent-guides)
(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
(setq highlight-indent-guides-method 'character)

;; diff hl config ;;
(require 'diff-hl)
(add-hook 'prog-mode-hook 'turn-on-diff-hl-mode)
(add-hook 'vc-dir-mode-hook 'turn-on-diff-hl-mode)

;; Markdown Mode config ;;
(add-hook 'gfm-mode 'flyspell-mode)

(add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'gfm-mode-hook 'turn-on-auto-fill)
(add-hook 'org-mode-hook 'turn-on-auto-fill)

;; Engine Mode config ;;
(require 'engine-mode)

(defengine duckduckgo
  "https://duckduckgo.com/?q=%s"
  :keybinding "d")

(defengine github
  "https://github.com/search?ref=simplesearch&q=%s"
  :keybinding "g")

(defengine google
  "http://www.google.com/search?ie=utf-8&oe=utf-8&q=%s")

(defengine stack-overflow
  "https://stackoverflow.com/search?q=%s"
  :keybinding "s")

(defengine wikipedia
  "http://www.wikipedia.org/search-redirect.php?language=en&go=Go&search=%s"
  :keybinding "w")

(defengine wiktionary
  "https://www.wikipedia.org/search-redirect.php?family=wiktionary&language=en&go=Go&search=%s")

(engine-mode 1)

;; Ivy Config ;;
(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(global-set-key "\C-s" 'swiper)
(global-set-key (kbd "C-c C-r") 'ivy-resume)
(global-set-key (kbd "<f6>") 'ivy-resume)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
(global-set-key (kbd "C-c g") 'counsel-git)
(global-set-key (kbd "C-c j") 'counsel-git-grep)
(global-set-key (kbd "C-c k") 'counsel-ag)
(global-set-key (kbd "C-x l") 'counsel-locate)
(global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
(define-key read-expression-map (kbd "C-r") 'counsel-expression-history)

;; SLIME Config ;;
(setq inferior-lisp-program "/usr/bin/sbcl")

;; Java Config ;;
(require 'eclim)
(add-hook 'java-mode-hook 'eclim-mode)
(custom-set-variables
 '(eclim-executable "~/Programs/eclim.jar"))

; ################################ ;
;;;; Emacs Config No Man's Land ;;;;
; ###############################  ;

;; Notes for using my Emacs ;;
; Remember, to look up a function, use C-h f. This will allow you to look up functions. ;
